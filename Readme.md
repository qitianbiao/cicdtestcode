# steps

## image & container

1. docker pull tomcat:9-jdk8-corretto
2. docker pull maven:3-amazoncorretto-8
3. docker run -itd --name mvn-env maven:3-amazoncorretto-8 bash

## code

1. git clone http://172.16.99.4:10080/xumenghao/java-demo.git

## mvn build

1. docker cp java-demo mvn-env:/xmh/code/
2. docker exec -it mvn-env bash
3. mvn-env: `mvn package`或`mvn compile war:war`
4. mvn-env: `exit`
5. docker cp mvn-env:/xmh/code/java-demo/target/java-demo-1.0-SNAPSHOT.war .

## docker build

1. docker build . -t xmh-jd:1

## test

```sh
docker run -itd --name xmh-jd1 -p 8081:8080 xmh-jd:1

curl --location --request GET 'http://localhost:8081/java-demo-1.0-SNAPSHOT/TomcatDemo/DemoServlet' \
   --header 'Content-Type: text/plain' \
   --data 'your-data'
```
